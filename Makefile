install:
	npm install

run:
	ng serve --open

db-serve:
	json-server json-server/db.json --routes json-server/routes.json

db-bigdata-serve:
	json-server json-server/big-db.json --routes json-server/routes.json
