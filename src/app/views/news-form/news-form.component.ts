import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NewsService } from '../../services/NewsApi/news-api.service';

@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.css']
})
export class NewsFormComponent implements OnInit {
  newsForm: FormGroup;
  public categories = [
    {value: 'food', viewValue: 'Food'},
    {value: 'life', viewValue: 'Life'},
    {value: 'health', viewValue: 'Health'}
  ];

  constructor(
    private router: Router,
    private newsService: NewsService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  gotoNews() {
    this.router.navigate(['/news']);
  }

  async handleNewsCreate() {
    if (!this.newsForm.valid) {
      alert('Please, fill out all fields.');
      return;
    }

    const news = this.newsForm.getRawValue();
    await this.newsService.postNews(news);
    this.router.navigate(['/news']);
  }

  private createForm() {
    this.newsForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      category: new FormControl('', Validators.required)
    });
  }

}
