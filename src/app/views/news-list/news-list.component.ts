import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { News } from '../../interfaces/News';
import { NewsService } from '../../services/NewsApi/news-api.service';
import { MatTableDataSource, PageEvent } from '@angular/material';
import { NewsQueryParams } from '../../interfaces/NewsQueryParams';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {
  public news: MatTableDataSource<News>;
  public totalNews = 0;
  public categories = [
    {value: 'food', viewValue: 'Food'},
    {value: 'life', viewValue: 'Life'},
    {value: 'health', viewValue: 'Health'}
  ];


  public displayedColumns = ['title', 'category'];
  public queryParams: NewsQueryParams = this.getQueryFromStorage() || {
    page: 1,
    limit: 10
  };

  pageEvent: PageEvent;

  constructor(
    private newsService: NewsService,
    private router: Router
  ) {}

  async ngOnInit() {
    await this.getNews();
  }

  public async onPaginateChange(event) {
    const page = event.pageIndex + 1;
    const limit = event.pageSize;
    const queryParams = Object.assign({}, this.queryParams);

    this.queryParams = Object.assign(queryParams, {page, limit});
    this.saveQueryToStorage();

    await this.getNews();
  }

  public async onCategorySelectionChange(event) {
    const category = event.value;
    const queryParams = Object.assign({}, this.queryParams, { category, page: 1 });

    if (!category) {
      delete queryParams.category;
    }

    this.queryParams = queryParams;
    this.saveQueryToStorage();
    await this.getNews();
  }

  public gotoForm() {
    this.router.navigate(['/form']);
  }

  private async getNews() {
    this.news = new MatTableDataSource(await this.newsService.getNews(this.queryParams));
    this.totalNews = await this.newsService.getAllNewsLength(this.queryParams.category);
  }

  private saveQueryToStorage(): void {
    sessionStorage.setItem('query', JSON.stringify(this.queryParams));
  }

  private getQueryFromStorage(): NewsQueryParams {
    return JSON.parse(sessionStorage.getItem('query'));
  }
}
