import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { News } from '../../interfaces';
import { NewsService } from '../../services/NewsApi/news-api.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.css']
})
export class NewsDetailsComponent implements OnInit {
  newsItem: News;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private newsService: NewsService
  ) { }

  async ngOnInit() {
    const newsItemId = this.route.snapshot.params.id;
    this.newsItem = await this.newsService.getNewsItem(newsItemId);
  }

  gotoNews() {
    this.router.navigate(['/news']);
  }
}
