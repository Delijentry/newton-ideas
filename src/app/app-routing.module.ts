import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { NewsListComponent} from './views/news-list/news-list.component';
import { NewsDetailsComponent } from './views/news-details/news-details.component';
import { NewsFormComponent } from './views/news-form/news-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/news', pathMatch: 'full' },
  { path: 'news', component: NewsListComponent },
  { path: 'news/:id', component: NewsDetailsComponent },
  { path: 'form', component: NewsFormComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
