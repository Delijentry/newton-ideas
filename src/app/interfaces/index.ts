import { News } from './News';
import { NewsQueryParams } from './NewsQueryParams';

export {
  News,
  NewsQueryParams
};
