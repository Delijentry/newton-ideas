export interface NewsQueryParams {
  limit: number;
  page: number;
  category?: string;
}
