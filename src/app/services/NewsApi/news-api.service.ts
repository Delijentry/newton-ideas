import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News, NewsQueryParams } from '../../interfaces';


@Injectable()
export class NewsService {
  private apiUrl = 'http://localhost:3000/news';
  private defaultParams = {
    page: 1,
    limit: 20
  };

  constructor(
    private http: HttpClient
  ) {}

  public async getNews(queryParams: NewsQueryParams = this.defaultParams): Promise<News[]> {
    const {page, limit, category} = queryParams;

    let url = `${this.apiUrl}?_page=${page}&_limit=${limit}`;
    if (category) {
      url = `${url}&category=${category}`;
    }

    return await this.http.get<News[]>(url).toPromise();
  }

  public async getAllNewsLength(category?: string): Promise<number> {
    let url = this.apiUrl;
    if (category) {
      url = `${this.apiUrl}?category=${category}`;
    }

    return await this.http.get<News[]>(url).toPromise()
      .then(response => response.length);
  }

  public async getNewsItem(id: string): Promise<News> {
    const url = `${this.apiUrl}?id=${id}`;

    return await this.http.get<News[]>(url).toPromise()
      .then(response => response[0]);
  }

  public async postNews(news): Promise<any> {
    const postedNews = Object.assign(news, { id: this.generateGUID()});
    return await this.http.post(this.apiUrl, postedNews).toPromise();
  }

  private generateGUID() {
    const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }
}
