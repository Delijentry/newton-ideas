### NewtonIdeas (`~3-4h total time spent`)

1) To install project dependencies execute command
    ```
    make install
    ```

2) Then start `db` with
    ```
    make db-serve
    ```
    or
    ```
    make db-bigdata-serve
    ```

3) And now we can start agnular app in separate terminal window with command
    ```
    make run
    ```
    The app will started and automatically open browser at `localhost:4200`
